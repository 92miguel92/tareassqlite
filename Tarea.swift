//
//  Tarea.swift
//  TareasSQLite
//
//  Created by Master Móviles on 11/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation

struct Tarea{
    
    var id : Int
    var titulo : String
    var prioridad : Int
    var vencimiento : Date
    
    init(){
        id = -1
        titulo = "Tarea Nueva"
        prioridad = 0
        vencimiento = Date()
    }
    
    init(id: Int, titulo: String, prioridad: Int, vencimiento: Date){
        self.id = id
        self.titulo = titulo
        self.prioridad = prioridad
        self.vencimiento = vencimiento
    }
}
