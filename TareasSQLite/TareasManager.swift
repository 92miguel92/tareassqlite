//
//  TareasManager.swift
//  TareasSQLite
//
//  Created by Master Móviles on 11/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit

class TareasManager: DBManager {
    
    func listarTareas() -> [Tarea]{
        let querySQL = "SELECT * FROM Tareas ORDER BY vencimiento ASC"
        var statement : OpaquePointer?;
        var lista : [Tarea] = [];
        
        let result = sqlite3_prepare_v2(db, querySQL, -1, &statement, nil)
        if (result==SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                let id = sqlite3_column_int(statement, 0)
                let titulo = String(cString: sqlite3_column_text(statement, 1))
                let prioridad = sqlite3_column_int(statement, 2)
                let vencimiento = sqlite3_column_int(statement, 3)
                let tarea = Tarea(id: Int(id),titulo: titulo,prioridad: Int(prioridad),vencimiento: Date(timeIntervalSince1970: TimeInterval(vencimiento)))
                lista.append(tarea)
            }
        }
        sqlite3_finalize(statement);
        for p in lista {
            print("\(p.id) \(p.titulo) \(p.prioridad) \(p.vencimiento)")
        }
        
        return lista
    }
    
    func insertarTarea(tarea: Tarea) -> Bool {
        
        let querySQL = "INSERT INTO Tareas (titulo, prioridad, vencimiento) VALUES (?,?,?)"
        var statement : OpaquePointer?
        sqlite3_prepare_v2(db, querySQL, -1, &statement, nil)
        sqlite3_bind_text(statement, 1, tarea.titulo, -1, nil)
        sqlite3_bind_int(statement, 2, Int32(tarea.prioridad))
        sqlite3_bind_int(statement, 2, Int32(tarea.vencimiento.timeIntervalSince1970))
        let result = sqlite3_step(statement);
        if (result==SQLITE_DONE) {
            print("Registro almacenado OK");
            return true
        }else{
            return false
        }
    }
}
